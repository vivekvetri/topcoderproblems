##Overview:

This repository holds my solutions for TopCoders Practice Problems. Using this repo for tracking and improving my solutions.
I am not mentioning the problem statement as its sole right is with TopCoder. Please refer the below License.

Feal free to reach out to me, if you think of any optimizations to the existing solutions.

**Mail ID :** g.vivek14@gmail.com

##TopCoder license statement : 

_This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2003, TopCoder, Inc. All rights reserved. All the problems are copyrighted by TopCoder_
