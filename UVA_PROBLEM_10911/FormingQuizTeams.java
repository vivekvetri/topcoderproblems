import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Scanner;

public class FormingQuizTeams {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in).useDelimiter("\n");
        int test = 1;
        while(true) {
            int caseNo = sc.nextInt();
            if(caseNo == 0)
                break;
            int noOfStudents = 2 * caseNo;
            int[][] points = new int[noOfStudents][2];
            double[] dp = new double[(int)Math.pow(2,noOfStudents)+1];
            Arrays.fill(dp, -1);
            for(int i = 0; i < noOfStudents; i++) {
                String student[] = sc.next().split(" ");
                points[i][0] = Integer.parseInt(student[1]);
                points[i][1] = Integer.parseInt(student[2]);
            }
            double minMatch = findMinMatching(0, points, dp, noOfStudents);
            BigDecimal bd = new BigDecimal(minMatch);
            bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
            System.out.println("Case "+ (test++) + ": " +bd);
            print2DArray(points);
        }
    }

    private static double findMinMatching(int i, int[][] points, double[] dp, int m) {
        if (i == (1 << m) - 1)
            return 0;
        if (dp[i] != -1)
            return dp[i];
        else {
            double min = Double.MAX_VALUE;
            for (int j = 0; j < m; j++) {
                if ((i & (1 << j)) == 0) {
                    for (int k = j + 1; k < m; k++)
                        if ((i & (1 << k)) == 0) {
                            double x = calcDistance(j, k, points);
                            min = Math.min(
                                    min,
                                    x + findMinMatching(i | (1 << k) | (1 << j), points, dp, m));
                        }
                }
            }
            return min;
        }
    }

    private static void print2DArray(int[][] points) {
        for(int[] row : points) {
            System.out.print(" points = [ ");
            for (int elem : row){
                System.out.print(elem+" ");
            }
            System.out.print("]\n");
        }
    }

    private static double calcDistance(int j, int k, int[][] points){
	    return Math.sqrt((points[j][0] - points[k][0])
			    * (points[j][0] - points[k][0]) + (points[j][1] - points[k][1])
			    * (points[j][1] - points[k][1]));
    }
}

