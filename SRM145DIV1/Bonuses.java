/**
 ** Created by vivek on 10/4/16.
 **/

public class Bonuses {
    public int[] getDivision(int[] points) {

        int totalPoints = 0;
        boolean[] withExtraBonus = new boolean[points.length];
        int bonuses[] = new int[points.length];
        int availableBonus = 100;

        for (int point : points ) {
            totalPoints += point;
        }

        System.out.println("Total Points : "+totalPoints);
        System.out.println("Points : ");
        printArray(points);

        for(int i = 0; i < points.length; i++) {
            bonuses[i] = ( points[i] * 100 ) / totalPoints;
            availableBonus -= bonuses[i];
        }

        System.out.println("\nBonuses (without extra) : ");
        printArray(bonuses);
        System.out.println("\nTotal leftover bonus : "+ availableBonus);

        while(availableBonus > 0) {
            int emp = -1;
            int maxPoint = -1;
            for (int i = 0; i < points.length; i++) {
                if (!withExtraBonus[i] && (points[i] > maxPoint)) {
                    maxPoint = points[i];
                    emp = i;
                }
            }
            withExtraBonus[emp] = true;
            bonuses[emp] += 1;
            availableBonus--;
        }
        System.out.println("\nBonuses (with extra) : ");
        printArray(bonuses);
        return bonuses;
    }
    void printArray(int[] arr) {
        for(int a : arr) { System.out.print(a+"\t"); }
    }
}
