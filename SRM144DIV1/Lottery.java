/*
    TO-DO : 1. Use Priority Queue
            2. Use BigInteger to hold large values
            3. Replace factorial with binomial to reduce time complexity
 */
public class Lottery {
    public String[] sortByOdds(String[] rules) {

        if (rules.length == 0) return rules;

        double[] probability = new double[rules.length];

        for(int i=0; i < rules.length; i++){
            String[] rule = rules[i].split(":");
            String _tmp = rule[1];
            String[] tmp = _tmp.trim().split("\\s+");

            int c = getIntValue(tmp[0].trim());
            int b = getIntValue(tmp[1].trim());
            probability[i] = findProbability(rule[0],c,b,tmp[2].charAt(0)=='T',tmp[3].charAt(0)=='T');
            //System.out.println(" Probability of Rule No. "+(i+1)+" is "+probability[i]);
        }

        /* O(n^2) --- TO DO : Has to be improved */

        for(int i=0; i< probability.length; i++){
            for(int j=i+1; j< probability.length; j++)
            if(probability[j]>probability[i]){
                swap(rules,j,i);
                double temp = probability[j];
                probability[j] = probability[i];
                probability[i] = temp;
            }
            else if(probability[j]==probability[i]){
                String[] str1 = rules[i].split(":");
                String[] str2 = rules[j].split(":");
                if(str2[0].compareTo(str1[0])<0)
                    swap(rules,j,i);
            }

        }
        return rules;
    }

    public int getIntValue(String str){
        int sum = 0;
        for(int i=0;i<str.length();i++){
            sum += Character.getNumericValue(str.charAt(i)) * Math.pow(10,(str.length()-i-1));
        }
        return sum;
    }

    public void swap(String[] arr, int index1, int index2) {
        String tmp = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = tmp;
    }

    public double findProbability(String name, int choices, int blanks, boolean isSorted, boolean isUnique) {
       // System.out.println("\nRULE : "+name+"\t"+choices+"\t"+blanks+"\t"+isSorted+"\t"+isUnique);
        double prob = 0;
        double possibilities = 0;

        if(!isSorted && !isUnique) {
            possibilities = Math.pow(choices,blanks);
        }
        else if(isSorted && !isUnique)
        {
             /* C(n-1+k, n-1) */
            possibilities = (fact(choices+blanks-1))/(fact(choices-1)*fact(blanks));
        }
        else if(!isSorted && isUnique)
        {
            possibilities = fact(choices) / fact(choices-blanks);
        }
        else if(isSorted && isUnique)
        {
            /* C(n, n-k) */
            possibilities = fact(choices)/(fact(choices-blanks)*fact(blanks));
        }
        prob = 1.0 / possibilities;
        return prob;
    }

    /* TO DO : Has to use binomial instead of fact */
    public double fact(int n){
        double factorial = 1;
        for(int i=n;i>0;i--)
        {
            factorial *= i;
        }
        return factorial;
    }
}

