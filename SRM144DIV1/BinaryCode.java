public class BinaryCode {
	public String[] decode(String message) {
		String[] output={"NAN","NAN"};
		for ( int i = 0; i< message.length(); i++){
			int curr = Character.getNumericValue(message.charAt(i));
			if(curr > 3 || curr < 0 ) return output; 
		}

		if(message.length() <1 || message.length() >50) {
			return output;
		}

		String plain0 ="0";
		for ( int i = 1; i < message.length() && plain0 !="NONE" && plain0.length()!=message.length(); i++ ) { 
			int tmp = Character.getNumericValue(message.charAt(i-1)) - Character.getNumericValue(plain0.charAt(i-1));
			if((i-1)>0)
				tmp = tmp - Character.getNumericValue(plain0.charAt(i-2));
			if (tmp<0 || tmp>1) {
				plain0 = "NONE";
				continue;
			}   
			else
				plain0 += tmp;
		}   
		output[0] = plain0;
		String plain1 ="1";
		for ( int i = 1; i < message.length() && plain1 != "NONE" && plain1.length()!=message.length(); i++ ) { 
			int tmp = Character.getNumericValue(message.charAt(i-1)) - Character.getNumericValue(plain1.charAt(i-1));
			if((i-1)>0)
				tmp = tmp - Character.getNumericValue(plain1.charAt(i-2));
			if (tmp<0 || tmp>1) {
				plain1 = "NONE";
				continue;
			}   
			else
				plain1 += tmp;
		}   
		output[1] = plain1;
		return output;
	}
}
