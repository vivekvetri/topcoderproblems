public class Time {
	public String whatTime(int sec) {
		String time = "";
		int h = sec / 3600;
		sec -= h * 3600;
		int m = sec / 60;
		sec -= m * 60;
		time = h+":"+m+":"+sec;
		return time;
	}
}
