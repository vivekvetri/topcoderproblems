public class BinaryCode {

	public String[] decode(String message) {

		String[] output = { "NAN" , "NAN" };
		String element1 = "00";
		String element2 = "01";

		for ( int i = 0; i < message.length(); i++) {
			int curr = Character.getNumericValue(message.charAt(i));
			if(curr > 3 || curr < 0 ) {
				return output;
			}
		}

		if(message.length() < 1 || message.length() > 50) {
			return output;
		}

		output[0] = _decode(message,element1);
		output[1] = _decode(message,element2);
		return output;
	}

	public String _decode(String input, String plain)
	{
		for ( int i = 1; i <= input.length(); i++ ) {
			int tmp = Character.getNumericValue(input.charAt(i-1)) 
				- Character.getNumericValue(plain.charAt(i)) 
				- Character.getNumericValue(plain.charAt(i-1));
			if(tmp!=0 && tmp!=1) {
				return "NONE";
			}
			plain += tmp;
		}
		plain = plain.substring(1, input.length()+1);
		System.out.println(" Decrypted string : "+plain);
		return plain;
	}
}
